//
//  AppDelegate.swift
//  data-persistence
//
//  Created by Howard on 2021/7/20.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        self.window!.backgroundColor = UIColor.white
        
        let tabbar = UITabBarController()
        
        let controller1 = PreferenceController()
        
        let controller2 = PlistController()
        
        let controller3 = MyNotification()
        
        controller1.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 100)
        
        controller2.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 200)
        
        controller3.tabBarItem = UITabBarItem(tabBarSystemItem: .downloads, tag: 300)
        
        tabbar.viewControllers = [controller1, controller2, controller3]
        
        window?.rootViewController = tabbar
        
        window?.makeKeyAndVisible()
        return true
    }
}

