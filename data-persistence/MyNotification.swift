//
//  Notification.swift
//  data-persistence
//
//  Created by Howard on 2021/7/21.
//

import UIKit

// MARK: - 这里既担任观察者也担任主题
class MyNotification: UIViewController {
    
    override func viewDidLoad() {
        register()
    }
    override func viewDidAppear(_ animated: Bool) {
        post()
    }
    // MARK: - 观察者把自己注册到主题
    func register() {
        NotificationCenter.default.addObserver(self, selector: .testSelector, name: .testName, object: nil)
    }
    // MARK: - 主题变动（调用所有以注册的观察者当初注册的钩子）
    func post() {
        NotificationCenter.default.post(name: .testName, object: nil, userInfo: ["msg": "success"])
    }
    // MARK: - 谁组册谁注销
    deinit {
        // 移除通知
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: .testName, object: nil)
    }
    // MARK: - 钩子
    @objc func test(notification: Notification) {
        guard let userInfo = notification.userInfo,
              let message = userInfo["msg"] as? String else {
            print("No userInfo")
            return
        }
        let alert = UIAlertController(title: "Notification!",
                                          message:"\(message)",
                                          preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension Notification.Name {
    static let testName = Notification.Name(rawValue: "test")
}
extension Selector {
    // 哪个类调用哪个类extension
    static let testSelector = #selector(MyNotification.test)
}
