//
//  Plist.swift
//  data-persistence
//
//  Created by Howard on 2021/7/20.
//
import UIKit

class Plist {
    private let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    
    private var fullName: String!
    
    private var map: NSMutableDictionary?
    
    init(fileName: String) {
        setFileName(fullName: fileName)
        self.load()
    }
    // MARK: - 设置要读取的文件
    func setFileName(fullName: String) {
        self.fullName = fullName
    }
    // MARK: - 读取本地
    private func load() {
        print(documentPath + "/" + fullName)
        map = NSMutableDictionary(contentsOfFile: documentPath + "/" + fullName) ?? NSMutableDictionary()
    }
    // MARK: - 获取某一行或失败
    func read(key: NSString) -> Any?{
        return map?[key]
    }
    // MARK: - 增加一行或覆盖一行
    func write(key: NSString, value: NSObject) {
        map?[key] = value
        // 自动保存
        save()
    }
    // MARK: - 保存到本地
    func save() {
        let bl = map?.write(to: URL(fileURLWithPath: documentPath + "/" + fullName), atomically: true)
        print("write location \(String(describing: bl))")
    }
    // MARK: - 删除一行或失败
    func remove(key: NSString) {
        map?[key] = nil
    }
    // MARK: - 获取所有键
    func getKeyArray() -> [Any]? {
        return map?.allKeys
    }
    // MARK: - 获取所有值
    func getValueArray() -> [Any]? {
        return map?.allValues
    }
}
