//
//  UserDefaultsController.swift
//  data-persistence
//  用户偏好存储
//
//  Created by Howard on 2021/7/20.
//

import UIKit

class PreferenceController: UIViewController {

    let defaults = UserDefaults.standard
    
    let fullSize = UIScreen.main.bounds.size
    
    let label: UILabel!
    let textAccount: UITextField!
    let textPassword: UITextField!
    let button: UIButton!
    
    enum Preference: String {
        case account = "account"
        
        case password = "password"
    }
    init() {
        label = UILabel(frame: CGRect(x: 0, y: 0, width: fullSize.width, height: fullSize.height * 0.1))
        label.text = "auto save preference"
        
        
        textAccount = UITextField(frame: CGRect(x: 0, y: 0, width: fullSize.width * 0.5, height: fullSize.width * 0.1))
        textAccount.center = CGPoint(x: fullSize.width * 0.5, y: fullSize.height * 0.2)
        textAccount.placeholder = "please input account"
        textAccount.borderStyle = .line
        
        
        textPassword = UITextField(frame: CGRect(x: 0, y: 0, width: fullSize.width * 0.5, height: fullSize.width * 0.1))
        textPassword.center = CGPoint(x: fullSize.width * 0.5, y: fullSize.height * 0.4)
        textPassword.placeholder = "please input password"
        textPassword.borderStyle = .line
        
        
        button = UIButton(type: .contactAdd)
        button.center = CGPoint(x: fullSize.width * 0.5, y: fullSize.height * 0.7)
        button.addTarget(nil, action: #selector(save), for: .touchUpInside)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(label)
        if let text = defaults.string(forKey: Preference.account.rawValue) {
            textAccount.text = text
        }
        view.addSubview(textAccount)
        if let text = defaults.string(forKey: Preference.password.rawValue) {
            textPassword.text = text
        }
        view.addSubview(textPassword)
        view.addSubview(button)
    }
    
    @objc func save() {
        defaults.setValue(textAccount.text, forKey: Preference.account.rawValue)
        defaults.setValue(textPassword.text, forKey: Preference.password.rawValue)
    }
}
