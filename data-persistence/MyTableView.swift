//
//  MyTableView.swift
//  MyTableView
//
//  Created by Howard on 2021/7/18.
//

import Foundation
import UIKit

class MyTableView: UITableView {
    
    struct TableGound {
        var name: String
        var arr: [String]
    }
    
    private var data: [TableGound] = []

    public override init(frame: CGRect, style: UITableView.Style) {
        
        super.init(frame: frame, style: style)
        
        super.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        delegate = self
        
        dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - 获取某组数据
    func getData(section: Int) -> [Any]{
        return data[section].arr
    }
    // MARK: - 获取某项数据
    func getData(section: Int, row: Int) -> Any {
        return data[section].arr[row]
    }
    // MARK: - 获取全部数据
    func getData() -> [TableGound]{
        return self.data
    }
    // MARK: - 设置数组内容
    func load(data: [TableGound]) {
        self.data = data
    }
    // MARK: - 设置无名数组内容
    func load(array: [[String]]) {
        for (index,arr) in array.enumerated() {
            self.data.append(TableGound(name: "Gound\(index)", arr: arr))
        }
    }
    // MARK: - 添加数据
    func addData(_ value: String, section: Int = 0, row: Int = 0) {
        data[section].arr.insert(value, at: row)
        // 自动更新界面
        self.beginUpdates()
        self.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
        self.endUpdates()
    }
}

extension MyTableView: UITableViewDelegate {
    // MARK: - 点击调用
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // 消除选中状态
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension MyTableView: UITableViewDataSource {
    // MARK: - 移动调用
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let sourceSection = sourceIndexPath.section
        let destinationSection = destinationIndexPath.section
        let sourceValue = data[sourceSection].arr[sourceIndexPath.row]
        if sourceSection == destinationSection && sourceIndexPath.row == destinationIndexPath.row {
        }else{
            data[sourceSection].arr.remove(at: sourceIndexPath.row)
            data[destinationSection].arr.insert(sourceValue, at: destinationIndexPath.row)
        }
    }
    //MARK: - 编辑行
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // 删除数据源一行
            self.data[indexPath.section].arr.remove(at: indexPath.row)
            // 删除界面一行
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
        }
    }
    
    //MARK: - 设置section标题
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return data[section].name
    }
    
    //MARK: - 是否开启编辑行（增/删）
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //MARK: - 返回每组多少个单元
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].arr.count
    }
    
    //MARK: - 定制每个表格的样式并返回
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        cell.accessoryType = .none
        // 显示内容
        if let myLabel = cell.textLabel {
            myLabel.text = "\(data[indexPath.section].arr[indexPath.row])"
        }
        // 选中背景变色
        cell.selectedBackgroundView = UIView()
        cell.selectedBackgroundView?.backgroundColor = .green
        // 选中显示颜色
        cell.textLabel?.highlightedTextColor = .red
        return cell
    }
    
    //MARK: - 设置组
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
}
