//
//  PlistController.swift
//  data-persistence
//  plist存储
//
//  Created by Howard on 2021/7/20.
//

import UIKit

class PlistController: UIViewController {
    
    let fullSize = UIScreen.main.bounds.size
    
    let textView:UITextView!
    
    let button: UIButton!
    
    let tableView: MyTableView!
    
    var plist:Plist?
    init() {
        textView = UITextView(frame: CGRect(x: 0, y: 0, width: 250, height: 190))
        textView.center = CGPoint(x: fullSize.width * 0.5, y: fullSize.height * 0.2)
        textView.backgroundColor = .darkGray
        textView.textColor = .white
        
        button = UIButton(type: .contactAdd)
        button.center = CGPoint(x: fullSize.width * 0.5, y: fullSize.height * 0.4)
        button.addTarget(nil, action: #selector(addInPlist), for: .touchUpInside)
        
        tableView = MyTableView(frame: CGRect(x: 0, y: 0, width: fullSize.width, height: fullSize.height * 0.5))
        tableView.center = CGPoint(x: fullSize.width * 0.5, y: fullSize.height * 0.75)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        view.addSubview(textView)
        view.addSubview(button)
        view.addSubview(tableView)
        plist = Plist(fileName: "abc.plist")
        let value = plist?.getValueArray() ?? [Any]()
        tableView.load(array: [value as! [String]])
    }
    
    @objc func addInPlist() {
        tableView.addData(textView.text)
        plist?.write(key: NSDate().description as NSString, value: NSString(string: textView.text))
    }
}
